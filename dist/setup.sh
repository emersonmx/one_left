#! /bin/bash

if test $# != 1
then
    echo "Usage: $0 <ogre_lib_path>"
else
    wget -c http://dl.dropbox.com/u/98764542/Programas/one_left/one_left_media.tar.bz2
    tar -xvf one_left_media.tar.bz2

    mkdir -p plugin/

    ln -sf $1/Plugin*.so plugin/
    ln -sf $1/RenderSystem*.so plugin/
fi
