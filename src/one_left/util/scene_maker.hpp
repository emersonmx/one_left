/*
  Copyright 2012, 2013 Emerson Max de Medeiros Silva

  This file is part of one_left.

  one_left is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  one_left is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with one_left.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ONE_LEFT_UTIL_SCENE_MAKER_HPP_
#define ONE_LEFT_UTIL_SCENE_MAKER_HPP_

namespace Ogre {

class Root;

} /* namespace Ogre */

namespace one_left {

class SceneMaker {
    public:
        virtual void Make(Ogre::Root* root) = 0;
};

} /* namespace one_left */
#endif /* ONE_LEFT_UTIL_SCENE_MAKER_HPP_ */

