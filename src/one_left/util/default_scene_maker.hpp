/*
  Copyright 2013 Emerson Max de Medeiros Silva

  This file is part of one_left.

  one_left is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  one_left is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with one_left.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ONE_LEFT_UTIL_DEFAULT_SCENE_MAKER_HPP_
#define ONE_LEFT_UTIL_DEFAULT_SCENE_MAKER_HPP_

#include <string>
#include <vector>

#include <boost/property_tree/ptree.hpp>

#include "one_left/util/scene_maker.hpp"

namespace Ogre {

class SceneManager;
class SceneNode;
class Entity;
class Light;
class Camera;

} /* namespace Ogre */

namespace one_left {

class DefaultSceneMaker : public SceneMaker {
    public:
        typedef struct api_version_t {
            uint8_t major;
            uint8_t minor;
            uint8_t patch;
        } APIVersion;

        static inline int VersionToNumber(const APIVersion& version) {
            return (version.major * 1000000) + (version.minor * 1000) +
                version.patch;
        }

        static std::string MakeVersionString(const APIVersion& version);

        DefaultSceneMaker(const std::string& filename);

        virtual void Make(Ogre::Root* root);

    protected:
        void CheckVersion(const APIVersion& supported_version);

        virtual Ogre::SceneManager*
        CreateScene(const boost::property_tree::ptree& scene_data);

        void CreateSceneNode(Ogre::SceneManager* scene_manager,
                             Ogre::SceneNode* parent,
                             const boost::property_tree::ptree& node_data);

        Ogre::SceneNode* CreateNode(Ogre::SceneNode* parent,
                const boost::property_tree::ptree& node_data);

        Ogre::Entity* CreateEntity(Ogre::SceneManager* scene_manager,
                const boost::property_tree::ptree& entity_data);

        Ogre::Light* CreateLight(Ogre::SceneManager* scene_manager,
                const boost::property_tree::ptree& light_data);

        void Configure(Ogre::Light* light,
                       const boost::property_tree::ptree& light_data);

        Ogre::Camera* CreateCamera(Ogre::SceneManager* scene_manager,
                const boost::property_tree::ptree& camera_data);

        void Configure(Ogre::Camera* camera,
                       const boost::property_tree::ptree& camera_data);

        virtual void ProcessScenes();

        virtual void
        ProcessScene(const boost::property_tree::ptree& scene_data);

        virtual void
        ProcessEnvironment(Ogre::SceneManager* scene_manager,
                           const boost::property_tree::ptree& environment_data);

        virtual void ProcessNodes(Ogre::SceneManager* scene_manager,
                                  Ogre::SceneNode* parent,
                                  const boost::property_tree::ptree& nodes);

        virtual void ProcessFog(Ogre::SceneManager* scene_manager,
                                const boost::property_tree::ptree& fog_data);

        Ogre::Root* root_;
        boost::property_tree::ptree tree_;
};

} /* namespace one_left */
#endif /* ONE_LEFT_UTIL_DEFAULT_SCENE_MAKER_HPP_*/

