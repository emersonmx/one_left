/*
  Copyright 2013 Emerson Max de Medeiros Silva

  This file is part of one_left.

  one_left is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  one_left is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with one_left.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <sstream>
#include <stdexcept>

#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>

#include <OgreRoot.h>
#include <OgreSceneNode.h>
#include <OgreEntity.h>
#include <OgreLight.h>

#include "one_left/util/default_scene_maker.hpp"

#include "one_left/util/exception/api_exception.hpp"

using namespace std;
using namespace boost::property_tree;

namespace one_left {

DefaultSceneMaker::DefaultSceneMaker(const string& filename)
        : root_(NULL) {

    read_json(filename, tree_);

    APIVersion version = { 1, 0, 0 };

    CheckVersion(version);
}

Ogre::Vector3 GetVector(const ptree& vector_data) {
    Ogre::Real x = vector_data.get<Ogre::Real>("x");
    Ogre::Real y = vector_data.get<Ogre::Real>("y");
    Ogre::Real z = vector_data.get<Ogre::Real>("z");

    return Ogre::Vector3(x, y, z);
}

Ogre::Quaternion GetQuaternion(const ptree& quaternion_data) {
    Ogre::Real w = quaternion_data.get<Ogre::Real>("w");
    Ogre::Real x = quaternion_data.get<Ogre::Real>("x");
    Ogre::Real y = quaternion_data.get<Ogre::Real>("y");
    Ogre::Real z = quaternion_data.get<Ogre::Real>("z");

    return Ogre::Quaternion(w, x, y, z);
}

boost::optional<Ogre::Vector3> GetPosition(const ptree& position_data) {
    boost::optional<const ptree&> position =
        position_data.get_child_optional("position");
    if (position) {
        return Ogre::Vector3(GetVector(*position));
    }

    return boost::optional<Ogre::Vector3>();
}

boost::optional<Ogre::Quaternion> GetRotation(const ptree& rotation_data) {
    boost::optional<const ptree&> rotation =
        rotation_data.get_child_optional("rotation");
    if (rotation) {
        return GetQuaternion(*rotation);
    }

    return boost::optional<Ogre::Quaternion>();
}

boost::optional<Ogre::Vector3> GetScale(const ptree& scale_data) {
    boost::optional<const ptree&> scale =
        scale_data.get_child_optional("scale");
    if (scale) {
        return GetVector(*scale);
    }

    return boost::optional<Ogre::Vector3>();
}

Ogre::ColourValue GetColor(const ptree& color_data) {
    Ogre::Real red = color_data.get<Ogre::Real>("red");
    Ogre::Real green = color_data.get<Ogre::Real>("green");
    Ogre::Real blue = color_data.get<Ogre::Real>("blue");
    boost::optional<Ogre::Real> alpha =
        color_data.get_optional<Ogre::Real>("alpha");

    if (alpha) {
        return Ogre::ColourValue(red, green, blue, *alpha);
    }

    return Ogre::ColourValue(red, green, blue);
}

Ogre::Radian GetAngle(const string& value) {
    istringstream buffer(value);
    Ogre::Real real = 0.0f;
    string type = "°";

    buffer >> real >> type;

    if (type == "°") {
        return Ogre::Degree(real);
    } else if (type == "rad") {
        return Ogre::Radian(real);
    }

    return Ogre::Radian(real);
}

string DefaultSceneMaker::MakeVersionString(const APIVersion& version) {
    stringstream version_string;

    version_string << int(version.major) << "." << int(version.minor) << "."
                   << int(version.patch);

    return version_string.str();
}

void DefaultSceneMaker::Make(Ogre::Root* root) {
    if (root == NULL) {
        throw logic_error("Impossível criar a cena. root não pode ser NULL");
    }

    root_ = root;

    ProcessScenes();
}

void DefaultSceneMaker::CheckVersion(const APIVersion& supported_version) {
    string string_version = tree_.get<string>("api_version");

    int major = 0, minor = 0, patch = 0;

    istringstream buffer(string_version);
    string token;
    for (int i = 0; getline(buffer, token, '.'); i++) {
        istringstream to_real(token);
        if (i == 0) {
            to_real >> major;
        } else if (i == 1) {
            to_real >> minor;
        } else if (i == 2) {
            to_real >> patch;
        }
    }

    APIVersion file_version = { major, minor, patch };

    if (VersionToNumber(file_version) > VersionToNumber(supported_version)) {
        throw  APIException("Versão do arquivo > Versão suportada (" +
                            MakeVersionString(file_version) + " > " +
                            MakeVersionString(supported_version) + ")");
    }
}

Ogre::SceneManager* DefaultSceneMaker::CreateScene(const ptree& scene_data) {
    boost::optional<string> type = scene_data.get_optional<string>("type");
    string name = scene_data.get<string>("name", Ogre::StringUtil::BLANK);
    Ogre::SceneManager* scene_manager = NULL;

    if (type) {
        scene_manager = root_->createSceneManager(*type, name);
    } else {
        scene_manager = root_->createSceneManager(Ogre::ST_GENERIC, name);
    }

    return scene_manager;
}

void DefaultSceneMaker::CreateSceneNode(Ogre::SceneManager* scene_manager,
                                        Ogre::SceneNode* parent,
                                        const ptree& node_data) {

    string type = node_data.get<string>("type");
    if (type == "node") {
        Ogre::SceneNode* scene_node = CreateNode(parent, node_data);

        ProcessNodes(scene_manager, scene_node, node_data.get_child("children",
                                                                    ptree()));
    } else if (type == "entity") {
        Ogre::SceneNode* scene_node = CreateNode(parent, node_data);

        Ogre::Entity* entity =
            CreateEntity(scene_manager, node_data.get_child("mesh"));

        scene_node->attachObject(entity);
        ProcessNodes(scene_manager, scene_node, node_data.get_child("children",
                                                                    ptree()));
    } else if (type == "light") {
        Ogre::Light* light = CreateLight(scene_manager, node_data);

        parent->attachObject(light);
    } else if (type == "camera") {
        Ogre::Camera* camera = CreateCamera(scene_manager, node_data);

        parent->attachObject(camera);
    }
}

Ogre::SceneNode* DefaultSceneMaker::CreateNode(Ogre::SceneNode* parent,
                                               const ptree& node_data) {

    Ogre::SceneNode* scene_node = NULL;

    boost::optional<string> name = node_data.get_optional<string>("name");
    if (name) {
        scene_node = parent->createChildSceneNode(*name);
    } else {
        scene_node = parent->createChildSceneNode();
    }

    boost::optional<Ogre::Vector3> position = GetPosition(node_data);
    if (position) {
        scene_node->setPosition(*position);
    }

    boost::optional<Ogre::Quaternion> rotation = GetRotation(node_data);
    if (rotation) {
        scene_node->setOrientation(*rotation);
    }

    boost::optional<Ogre::Vector3> scale = GetScale(node_data);
    if (scale) {
        scene_node->setScale(*scale);
    }

    return scene_node;
}

Ogre::Entity* DefaultSceneMaker::CreateEntity(Ogre::SceneManager* scene_manager,
                                              const ptree& entity_data) {

    string name = entity_data.get<string>("name", "");
    string file = entity_data.get<string>("file");
    string group = entity_data.get<string>("group", "");
    bool cast_shadow = entity_data.get<bool>("cast_shadow", false);

    Ogre::Entity* entity = NULL;

    if (name != "") {
        if (group != "") {
            entity = scene_manager->createEntity(name, file, group);
        } else {
            entity = scene_manager->createEntity(name, file);
        }
    } else {
        entity = scene_manager->createEntity(file);
    }

    if (cast_shadow) {
        entity->setCastShadows(cast_shadow);
    }

    return entity;
}

Ogre::Light* DefaultSceneMaker::CreateLight(Ogre::SceneManager* scene_manager,
                                            const ptree& light_data) {

    boost::optional<string> name = light_data.get_optional<string>("name");
    Ogre::Light* light = NULL;
    if (name) {
        light = scene_manager->createLight(*name);
    } else {
        light = scene_manager->createLight();
    }

    boost::optional<Ogre::Vector3> position = GetPosition(light_data);
    if (position) {
        light->setPosition(*position);
    }

    boost::optional<const ptree&> configuration =
        light_data.get_child_optional("configuration");

    if (configuration) {
        Configure(light, *configuration);
    }

    return light;
}

void DefaultSceneMaker::Configure(Ogre::Light* light, const ptree& light_data) {

    boost::optional<string> type =
        light_data.get_optional<string>("type");
    if (type) {
        Ogre::Light::LightTypes light_type = Ogre::Light::LT_POINT;
        if (*type == "point") {
            light_type = Ogre::Light::LT_POINT;
        } else if (*type == "directional") {
            light_type = Ogre::Light::LT_DIRECTIONAL;
        } else if (*type == "spotlight") {
            light_type = Ogre::Light::LT_SPOTLIGHT;

            boost::optional<const ptree&> range =
                light_data.get_child_optional("range");
            if (range) {
                Ogre::Radian inner = GetAngle((*range).get<string>("inner"));
                Ogre::Radian outer = GetAngle((*range).get<string>("outer"));
                Ogre::Real falloff = (*range).get<Ogre::Real>("falloff");

                light->setSpotlightRange(inner, outer, falloff);
            }
        }

        light->setType(light_type);
    }

    boost::optional<bool> visible =
        light_data.get_optional<bool>("visible");
    if (visible) {
        light->setVisible(*visible);
    }

    boost::optional<bool> cast_shadow =
        light_data.get_optional<bool>("cast_shadow");
    if (cast_shadow) {
        light->setCastShadows(*cast_shadow);
    }

    boost::optional<const ptree&> diffuse_color_data =
        light_data.get_child_optional("diffuse_color");
    if (diffuse_color_data) {
        Ogre::ColourValue diffuse_color = GetColor(*diffuse_color_data);
        light->setDiffuseColour(diffuse_color.r, diffuse_color.g,
                                diffuse_color.b);
    }

    boost::optional<const ptree&> specular_color_data =
        light_data.get_child_optional("specular_color");
    if (specular_color_data) {
        Ogre::ColourValue specular_color = GetColor(*specular_color_data);
        light->setSpecularColour(specular_color.r, specular_color.g,
                                 specular_color.b);
    }

    boost::optional<const ptree&> attenuation =
        light_data.get_child_optional("attenuation");
    if (attenuation) {
        Ogre::Real range = (*attenuation).get<Ogre::Real>("range");
        Ogre::Real constant = (*attenuation).get<Ogre::Real>("constant");
        Ogre::Real linear = (*attenuation).get<Ogre::Real>("linear");
        Ogre::Real quadratic = (*attenuation).get<Ogre::Real>("quadratic");

        light->setAttenuation(range, constant, linear, quadratic);
    }
}

Ogre::Camera* DefaultSceneMaker::CreateCamera(Ogre::SceneManager* scene_manager,
                                              const ptree& camera_data) {

    string name = camera_data.get<string>("name");
    Ogre::Camera* camera = scene_manager->createCamera(name);

    boost::optional<Ogre::Vector3> position = GetPosition(camera_data);
    if (position) {
        camera->setPosition(*position);
    }

    boost::optional<Ogre::Quaternion> rotation = GetRotation(camera_data);
    if (rotation) {
        camera->setOrientation(*rotation);
    }

    boost::optional<const ptree&> configuration =
        camera_data.get_child_optional("configuration");

    if (configuration) {
        Configure(camera, *configuration);
    }

    return camera;
}

void DefaultSceneMaker::Configure(Ogre::Camera* camera,
                                  const ptree& camera_data) {

    Ogre::Radian fov = GetAngle(camera_data.get<string>("fov", "45°"));
    camera->setFOVy(fov);

    boost::optional<string> projection =
        camera_data.get_optional<string>("projection");
    if (projection) {
        Ogre::ProjectionType projection_type = Ogre::PT_PERSPECTIVE;
        if (*projection == "perspective") {
            projection_type = Ogre::PT_PERSPECTIVE;
        } else if (*projection == "orthographic") {
            projection_type = Ogre::PT_ORTHOGRAPHIC;
        }
        camera->setProjectionType(projection_type);
    }

    boost::optional<const ptree&> clipping =
        camera_data.get_child_optional("clipping");
    if (clipping) {
        boost::optional<Ogre::Real> near =
            (*clipping).get_optional<Ogre::Real>("near");
        if (near) {
            camera->setNearClipDistance(*near);
        }

        boost::optional<Ogre::Real> far =
            (*clipping).get_optional<Ogre::Real>("far");
        if (far) {
            camera->setFarClipDistance(*far);
        }
    }
}

void DefaultSceneMaker::ProcessScenes() {
    BOOST_FOREACH(const ptree::value_type& scene,
                  tree_.get_child("scenes")) {

        ProcessScene(scene.second);
    }
}

void DefaultSceneMaker::ProcessScene(const ptree& scene_data) {
    Ogre::SceneManager* scene_manager = NULL;

    scene_manager = CreateScene(scene_data.get_child("scene", ptree()));

    ProcessEnvironment(scene_manager, scene_data.get_child("environment", ptree()));

    ProcessNodes(scene_manager, scene_manager->getRootSceneNode(),
                 scene_data.get_child("nodes", ptree()));
}

void DefaultSceneMaker::ProcessEnvironment(Ogre::SceneManager* scene_manager,
                                           const ptree& environment_data) {

    BOOST_FOREACH(const ptree::value_type& property, environment_data) {
        const string& key = property.first;
        const ptree& value = property.second;

        if (key == "ambient_color") {
            Ogre::ColourValue ambient_color = GetColor(value);
            scene_manager->setAmbientLight(ambient_color);
        } else if (key == "fog") {
            ProcessFog(scene_manager, value);
        }
    }
}

void DefaultSceneMaker::ProcessNodes(Ogre::SceneManager* scene_manager,
                                     Ogre::SceneNode* parent,
                                     const ptree& nodes) {

    BOOST_FOREACH(const ptree::value_type& node, nodes) {
        CreateSceneNode(scene_manager, parent, node.second);
    }
}

void DefaultSceneMaker::ProcessFog(Ogre::SceneManager* scene_manager,
                                   const ptree& fog_data) {

    string mode_string = fog_data.get<string>("mode", "none");
    Ogre::FogMode mode = Ogre::FOG_NONE;

    if (mode_string == "none") {
        mode = Ogre::FOG_NONE;
    } else if (mode_string == "exp") {
        mode = Ogre::FOG_EXP;
    } else if (mode_string == "exp2") {
        mode = Ogre::FOG_EXP2;
    } else if (mode_string == "linear") {
        mode = Ogre::FOG_LINEAR;
    }

    Ogre::ColourValue color =
        GetColor(fog_data.get_child("color", ptree()));

    Ogre::Real density = fog_data.get<Ogre::Real>("density", 0.001f);
    Ogre::Real start = fog_data.get<Ogre::Real>("start", 0.0f);
    Ogre::Real end = fog_data.get<Ogre::Real>("end", 1.0f);

    scene_manager->setFog(mode, color, density, start, end);
}

} /* namespace one_left */

