/*
  Copyright 2012, 2013 Emerson Max de Medeiros Silva

  This file is part of one_left.

  one_left is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  one_left is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with one_left.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ONE_LEFT_APPLICATION_GAME_APPLICATION_HPP_
#define ONE_LEFT_APPLICATION_GAME_APPLICATION_HPP_

#include <OgreWindowEventUtilities.h>

#include "mxgame/application/application.hpp"
#include "one_left/application/ogre/ogre_startup.hpp"

namespace Ogre {

class Root;
class RenderWindow;
class SceneManager;

} /* namespace Ogre */

namespace mxgame {

class Timer;
class Clock;

} /* namespace mxgame */

namespace one_left {

class SceneMaker;

class OneLeftApplication
        : public mxgame::Application,
          public OgreStartup,
          public Ogre::WindowEventListener {

    public:
        OneLeftApplication();

        virtual void windowResized(Ogre::RenderWindow* render_window);

        virtual void windowClosed(Ogre::RenderWindow* render_window);

    protected:
        virtual void Initialize();

        void Finalize() throw ();

        virtual void SetupRoot();

        virtual void SetupResources();

        virtual void SetupRenderSystem();

        virtual void SetupRenderWindow();

        virtual void InitializeResources();

        virtual void SetupScene();

        virtual void ProcessEvents();

        virtual void Update();

        virtual void Render();

    private:
        mxgame::Timer* timer_;
        mxgame::Clock* clock_;

        SceneMaker* scene_maker_;

        Ogre::Root* root_;
        Ogre::RenderWindow* window_;
        Ogre::SceneManager* scene_manager_;
};

} /* namespace one_left */
#endif /* ONE_LEFT_APPLICATION_GAME_APPLICATION_HPP_ */

