/*
  Copyright 2012, 2013 Emerson Max de Medeiros Silva

  This file is part of one_left.

  one_left is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  one_left is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with one_left.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <OgreRoot.h>
#include <OgreRenderWindow.h>
#include <OgreWindowEventUtilities.h>
#include <OgreConfigFile.h>

#include "mxgame/system/time/system_timer.hpp"
#include "mxgame/system/time/clock.hpp"

#include "one_left/util/default_scene_maker.hpp"

#include "one_left/application/ogre/exception/setup_render_system_exception.hpp"
#include "one_left/application/ogre/exception/setup_render_window_exception.hpp"

#include "one_left/application/one_left_application.hpp"

namespace one_left {

OneLeftApplication::OneLeftApplication()
        : timer_(NULL), clock_(NULL), scene_maker_(NULL), root_(NULL),
          window_(NULL), scene_manager_(NULL) {}

void OneLeftApplication::windowResized(Ogre::RenderWindow* render_window) {
    Ogre::Camera* camera = scene_manager_->getCamera("default");
    camera->setAspectRatio(Ogre::Real(render_window->getWidth()) /
                           Ogre::Real(render_window->getHeight()));
}

void OneLeftApplication::windowClosed(Ogre::RenderWindow* render_window) {
    Exit();
}

void OneLeftApplication::Initialize() {
    // Impede de imprimir no terminal
    Ogre::LogManager* logger_ = new Ogre::LogManager();
    if (logger_ != NULL) {
        logger_->createLog("Ogre.log", true, false, false);
    }

    OgreStartup::Startup();

    timer_ = new mxgame::SystemTimer();
    clock_ = new mxgame::Clock(timer_);
    clock_->set_framerate(60);
}

void OneLeftApplication::Finalize() throw() {
    Ogre::WindowEventUtilities::removeWindowEventListener(window_, this);

    delete scene_maker_;
    delete clock_;
    delete timer_;
    delete root_;
}

void OneLeftApplication::SetupRoot() {
    root_ = new Ogre::Root();
}

void OneLeftApplication::SetupResources() {
    Ogre::ConfigFile config;

    config.load("resources.cfg");

    Ogre::ConfigFile::SectionIterator section_iterator =
        config.getSectionIterator();

    Ogre::String section_name, type_name, arch_name;

    while (section_iterator.hasMoreElements()) {
        section_name = section_iterator.peekNextKey();

        Ogre::ConfigFile::SettingsMultiMap* settings =
            section_iterator.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator iterator;

        for (iterator = settings->begin();
                iterator != settings->end();
                ++iterator) {

            type_name = iterator->first;
            arch_name = iterator->second;

            Ogre::ResourceGroupManager::getSingleton().addResourceLocation(
                arch_name, type_name, section_name);
        }
    }
}

void OneLeftApplication::SetupRenderSystem() {
    if (!root_->restoreConfig()) {
        if (!root_->showConfigDialog()) {
            throw SetupRenderSystemException("A configuração foi cancelada.");
        }
    }
}

void OneLeftApplication::SetupRenderWindow() {
    window_ = root_->initialise(true, "One Left");
    if (window_ == NULL) {
        throw SetupRenderWindowException();
    }

    Ogre::WindowEventUtilities::addWindowEventListener(window_, this);
}

void OneLeftApplication::InitializeResources() {
    Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(5);
    Ogre::ResourceGroupManager::getSingleton()
        .initialiseAllResourceGroups();
}

void OneLeftApplication::SetupScene() {
    scene_maker_ = new DefaultSceneMaker("media/scene/scene.json");
    scene_maker_->Make(root_);

    scene_manager_ = root_->getSceneManager("default");

    Ogre::Camera* camera = scene_manager_->getCamera("default");

    // Isso é coisa de gameplay .-.
    Ogre::Viewport* viewport = window_->addViewport(camera);
    viewport->setBackgroundColour(Ogre::ColourValue(0, 0, 0));

    camera->setAspectRatio(Ogre::Real(window_->getWidth()) /
                           Ogre::Real(window_->getHeight()));

    // windowResized(window_);dd
}

void OneLeftApplication::ProcessEvents() {
    clock_->tick();

    Ogre::WindowEventUtilities::messagePump();
}

void OneLeftApplication::Update() {
    ProcessEvents();
}

void OneLeftApplication::Render() {
    root_->renderOneFrame();
}

} /* namespace one_left */

