/*
  Copyright 2012, 2013 Emerson Max de Medeiros Silva

  This file is part of one_left.

  one_left is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  one_left is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with one_left.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "one_left/application/one_left_application.hpp"

using namespace std;
using namespace mxgame;
using namespace one_left;

int main(int argc, char* argv[]) {
    Application* application = new OneLeftApplication();

    application->Run();
    int error_code = application->error_code();

    delete application;

#ifndef NDEBUG
    std::string error_name = "No Error";

    if (error_code == EXIT_FAILURE) {
        error_name = "Error";
    }

    cout << "Error code: " << error_code << " (" << error_name << ")\n";
#endif /* NDEBUG */

    return error_code;
}

